#include "stdio.h"
#include <sys/types.h>
#include <fcntl.h>
#include <pwd.h>
#include <string.h>

void main()
{
 char buf[100];
 char prompt;
 char home[40];
 struct passwd *psswd;
 psswd = getpwent();
 if (psswd == NULL) {
   return;
 }
 strcpy(home, psswd->pw_dir);
 strcat(home, "/.profile");
 printf("HOME--->%s\n", psswd->pw_dir);
 FILE* profile = fopen(home, "r");
 if (profile == NULL) {
   printf("Failed to open .profile\n");
 }
 else {
  while(!feof(profile))
  {
   fgets(buf, 9, profile); 
   printf("LINE--> %s <--\n", buf);
   if (strncmp(buf, "PROMPT", 6) == 0) {
     prompt = buf[7]; 
     printf("prompt is %c\n", prompt);
     break;
   }
  }
 }
 fclose(profile);
} 
